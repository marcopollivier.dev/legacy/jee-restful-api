package br.com.m4u.rh.controller;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.m4u.rh.domain.SmsMessage;

@Path("/sms")
public class SmsMessageControllerRS {

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response doIt(SmsMessage sms) throws JsonProcessingException {
		
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(sms);
		
		System.out.println("#############################");
		System.out.println("Este é um mock para Operadora");
		System.out.println("A requisição foi recebida com sucesso e o retorno é:");
		System.out.println(json);
		System.out.println("/n/n#############################");
		
		return Response.ok(json, MediaType.APPLICATION_JSON).build(); 
		
	}
	
}
