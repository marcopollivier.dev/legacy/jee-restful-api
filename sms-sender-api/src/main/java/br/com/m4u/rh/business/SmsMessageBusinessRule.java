package br.com.m4u.rh.business;

import br.com.m4u.rh.domain.SmsMessage;
import br.com.m4u.rh.exception.BusinessRuleException;

public class SmsMessageBusinessRule {

	public void checkValidity(SmsMessage sms) throws BusinessRuleException {
		if ( (sms.getDtValidity() != null) && (sms.getDtValidity().before(sms.getDtSend())) )
			throw new BusinessRuleException("Este sms está fora da validade.");

	}

}
