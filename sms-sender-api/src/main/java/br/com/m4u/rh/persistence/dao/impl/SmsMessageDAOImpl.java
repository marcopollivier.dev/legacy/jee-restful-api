package br.com.m4u.rh.persistence.dao.impl;

import javax.persistence.EntityManager;

import br.com.m4u.rh.domain.SmsMessage;
import br.com.m4u.rh.persistence.EntityManagerProducer;
import br.com.m4u.rh.persistence.dao.SmsMessageDAO;

public class SmsMessageDAOImpl implements SmsMessageDAO {

	//TODO injetar dependencia

	//@Inject
	EntityManager entityManager;
	
	public SmsMessageDAOImpl() {
		entityManager = EntityManagerProducer.newEntityManager();
	}
	
	public SmsMessage retrieve(Integer id) {
		SmsMessage finded = entityManager.find(SmsMessage.class, id);
		entityManager.close();
		return finded;
	}

	public SmsMessage create(SmsMessage sms) {
		entityManager.getTransaction().begin();    
		entityManager.persist(sms);
		entityManager.getTransaction().commit();  
	
		entityManager.close();
		
		return sms;
	}
	
}
