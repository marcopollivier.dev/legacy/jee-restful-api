package br.com.m4u.rh.rest.json;

public class ErrorPacket {

	private String error;
	
	public ErrorPacket(String error) {
		this.error = error;
	}
	
	public String getError() {
		return error;
	}
	
	public void setError(String error) {
		this.error = error;
	}
	
}
