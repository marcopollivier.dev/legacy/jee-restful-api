package br.com.m4u.rh.persistence;

import java.io.Serializable;

import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;

public class EntityManagerProducer implements Serializable {

	private static final long serialVersionUID = 1L;

	@PersistenceUnit
	private static EntityManagerFactory factory;

	@Produces
	public EntityManager createEntityManager() {
		return factory.createEntityManager();
	}
	
	public void closeEntityManager(@Disposes EntityManager manager) {
		if (manager.isOpen())
			manager.close();
	}
	
	//sem injecao
	public static EntityManager newEntityManager() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("smssender");
		return factory.createEntityManager();
	}
	
}