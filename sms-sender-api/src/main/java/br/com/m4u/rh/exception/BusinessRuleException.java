package br.com.m4u.rh.exception;

public class BusinessRuleException extends Exception {

	private static final long serialVersionUID = -9144980934422041909L;

	public BusinessRuleException() {
		super("Ocorreu um erro de Regra de Negócio");
	}
	
	public BusinessRuleException(String msg) {
		super(msg);
	}

    public BusinessRuleException(String msg, Throwable cause){
        super(msg, cause);
    }
	
}
