package br.com.m4u.rh.service.url;

public class ServiceURL {

	private static final String LOCALHOST = "http://localhost:8080/";
	
	public static final String OPERATOR_URL = LOCALHOST + "mock-operadora/rest/sms/";
	
}
