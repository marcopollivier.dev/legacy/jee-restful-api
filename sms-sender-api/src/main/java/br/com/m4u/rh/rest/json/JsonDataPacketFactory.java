package br.com.m4u.rh.rest.json;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.m4u.rh.util.JSONConverter;

public class JsonDataPacketFactory {

	public static Response createPacket(Object obj) {
		if(obj == null)
			return createSuccessPacket("{}");
		
		String json = null;
		try {
			json = JSONConverter.convertToString(obj);
		} catch (Exception e) {
			return createFailPacket(e);
		}
		
		return createSuccessPacket(json);
	}
	
	public static Response createSuccessPacket(String json) {
		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}
	
	public static Response createFailPacket(Exception ex) {
		String json = null;
		try {
			json = JSONConverter.convertToString(new ErrorPacket(ex.getMessage()));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}
	
}
