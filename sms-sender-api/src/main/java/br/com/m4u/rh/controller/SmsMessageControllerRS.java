package br.com.m4u.rh.controller;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.m4u.rh.domain.SmsMessage;
import br.com.m4u.rh.rest.json.JsonDataPacketFactory;
import br.com.m4u.rh.service.SmsMessageService;
import br.com.m4u.rh.service.impl.SmsMessageServiceImpl;

@Path("/sms")
public class SmsMessageControllerRS {

	//TODO injetar dependencia
	//@Inject
	private SmsMessageService service;
	
	public SmsMessageControllerRS() {
		service = new SmsMessageServiceImpl();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJSON(@PathParam("id") Integer id) {
		return JsonDataPacketFactory.createPacket(service.retrieve(id)); 
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(SmsMessage domain)  {
		try {
			return JsonDataPacketFactory.createPacket(service.create(domain));
		} catch (Exception e) {
			return JsonDataPacketFactory.createFailPacket(e);
		}
	}
	
}
