package br.com.m4u.rh.persistence.dao;

import br.com.m4u.rh.domain.SmsMessage;

public interface SmsMessageDAO {

	public SmsMessage retrieve(Integer id);
	public SmsMessage create(SmsMessage sms);
	
}
