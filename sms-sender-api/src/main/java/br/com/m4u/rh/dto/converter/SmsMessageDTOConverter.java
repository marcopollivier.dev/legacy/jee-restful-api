package br.com.m4u.rh.dto.converter;

import java.util.Date;

import br.com.m4u.rh.domain.SmsMessage;
import br.com.m4u.rh.dto.SmsMessageDTO;

public class SmsMessageDTOConverter {

	public static SmsMessageDTO toDTO(SmsMessage sms) {
		return new SmsMessageDTO(sms.getFrom(), sms.getTo(), sms.getBody());
	}
	
	public  static SmsMessage toDomain(SmsMessageDTO dto) {
		return toDomain(dto, null);

	}

	public  static SmsMessage toDomain(SmsMessageDTO dto, Date validity) {
		return new SmsMessage(dto.getFrom(), dto.getTo(), dto.getBody(), validity);
	}
	
}
