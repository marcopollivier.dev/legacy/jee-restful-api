package br.com.m4u.rh.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.m4u.rh.business.SmsMessageBusinessRule;
import br.com.m4u.rh.domain.SmsMessage;
import br.com.m4u.rh.dto.converter.SmsMessageDTOConverter;
import br.com.m4u.rh.exception.BusinessRuleException;
import br.com.m4u.rh.network.restful.HTTPRestConnection;
import br.com.m4u.rh.persistence.dao.SmsMessageDAO;
import br.com.m4u.rh.persistence.dao.impl.SmsMessageDAOImpl;
import br.com.m4u.rh.service.SmsMessageService;
import br.com.m4u.rh.service.url.ServiceURL;

public class SmsMessageServiceImpl implements SmsMessageService {

	//TODO Injetar dependencia
	SmsMessageDAO dao;
	HTTPRestConnection conn;
	SmsMessageBusinessRule rule;
	
	public SmsMessageServiceImpl() {
		dao = new SmsMessageDAOImpl();
		conn = new HTTPRestConnection();
		rule = new SmsMessageBusinessRule();
	}
	
	@Override
	public SmsMessage retrieve(Integer id) {
		return dao.retrieve(id);
	}

	@Override
	public SmsMessage create(SmsMessage sms) throws BusinessRuleException, JsonProcessingException {
		rule.checkValidity(sms);
		conn.applicationJSONConn(ServiceURL.OPERATOR_URL, SmsMessageDTOConverter.toDTO(sms));
		return dao.create(sms);
	}
	
}