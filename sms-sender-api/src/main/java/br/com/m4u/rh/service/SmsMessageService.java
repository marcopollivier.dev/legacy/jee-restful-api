package br.com.m4u.rh.service;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.m4u.rh.domain.SmsMessage;
import br.com.m4u.rh.exception.BusinessRuleException;

public interface SmsMessageService {

	public SmsMessage retrieve(Integer id);
	public SmsMessage create(SmsMessage sms) throws BusinessRuleException, JsonProcessingException;
	
}
