package br.com.m4u.rh.network.restful;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sun.research.ws.wadl.HTTPMethods;

import br.com.m4u.rh.util.JSONConverter;

public class HTTPRestConnection {

	public void applicationJSONConn (String url, String json) {
		try {
			URL object = new URL(url);
			
			HttpURLConnection con = (HttpURLConnection) object.openConnection();
		
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestMethod(HTTPMethods.PUT.name());
			
			OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
			wr.write(json);
			wr.flush();
			
			int HttpResult = con.getResponseCode();
			
			if (HttpResult == HttpURLConnection.HTTP_OK || HttpResult == HttpURLConnection.HTTP_CREATED) {
				// log
				System.out.println("Envio efetuado com sucesso!!!");
			} else if (HttpResult == HttpURLConnection.HTTP_NOT_FOUND) { // 404
				throw new RuntimeException("Mobile User not found");
			} else if (HttpResult == HttpURLConnection.HTTP_BAD_METHOD) { // 405
				throw new RuntimeException("Validation exception");
			} else if (HttpResult == HttpURLConnection.HTTP_INTERNAL_ERROR) { // 500
				throw new RuntimeException("Internal Server Error");
			} else {
				throw new RuntimeException("Ocorreu um erro ao tentar se conectar com o serviço da operadora");
			}
			
		} catch (IOException e) {
			throw new RuntimeException("Erro ao contectar com o serviço");
		}
	}
	
	public void applicationJSONConn (String url, Object obj) throws JsonProcessingException {
		String json = JSONConverter.convertToString(obj);
		applicationJSONConn(url, json);
	}
	
}
