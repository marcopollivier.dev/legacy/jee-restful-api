package br.com.m4u.rh.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name="SMSMESSAGE")
public class SmsMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@NotNull
	@Column(name="TXT_FROM")
	private String from;
	
	@NotNull
	@Column(name="TXT_TO")
	private String to;
	
	@NotNull
	@Size(max=160)
	@Column(name="MSG_BODY")
	private String body;
	
	@Temporal(TemporalType.DATE)
	@Column(name="DT_VAL")
	private Date dtValidity;
	
	@Temporal(TemporalType.DATE)
	@Column(name="DT_SEND")
	private Date dtSend;

	public SmsMessage() {
		super();
		this.dtSend = new Date();
	}

	public SmsMessage(String from, String to, String body) {
		this();
		this.from = from;
		this.to = to;
		this.body = body;
	}
	
	public SmsMessage(String from, String to, String body, Date dtValidity) {
		this(from, to, body);
		this.dtValidity = dtValidity;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
	public Date getDtValidity() {
		return dtValidity;
	}

	public void setDtValidity(Date dtValidity) {
		this.dtValidity = dtValidity;
	}

	@JsonIgnore
	public Date getDtSend() {
		return dtSend;
	}

	public void setDtSend(Date dtSend) {
		this.dtSend = dtSend;
	}
	
}