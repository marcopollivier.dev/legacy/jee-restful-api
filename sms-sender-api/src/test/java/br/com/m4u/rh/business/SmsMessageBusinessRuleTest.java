package br.com.m4u.rh.business;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import br.com.m4u.rh.domain.SmsMessage;
import br.com.m4u.rh.exception.BusinessRuleException;

public class SmsMessageBusinessRuleTest {

	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	private SmsMessageBusinessRule rule = new SmsMessageBusinessRule();

	@Test
	public void checkValidityOkTest() throws BusinessRuleException {
		Date dtValidity = Date.from(LocalDate.now().plusDays(5).atStartOfDay(ZoneId.systemDefault()).toInstant());
		
		SmsMessage sms = new SmsMessage("", "", "", dtValidity);
	
		rule.checkValidity(sms);
		
	}

	@Test
	public void checkValidityNotOkTest() throws BusinessRuleException  {
		thrown.expect(BusinessRuleException.class);
		thrown.expectMessage("Este sms está fora da validade.");
		
		Date dtValidity = Date.from(LocalDate.now().minusDays(5).atStartOfDay(ZoneId.systemDefault()).toInstant());
		
		SmsMessage sms = new SmsMessage("", "", "", dtValidity);
	
		rule.checkValidity(sms);
		
	}
	
}
