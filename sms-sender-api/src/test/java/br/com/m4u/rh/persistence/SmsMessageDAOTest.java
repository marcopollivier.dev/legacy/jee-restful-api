package br.com.m4u.rh.persistence;

import static org.junit.Assert.assertNotNull;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import br.com.m4u.rh.domain.SmsMessage;
import br.com.m4u.rh.persistence.dao.SmsMessageDAO;
import br.com.m4u.rh.persistence.dao.impl.SmsMessageDAOImpl;

public class SmsMessageDAOTest {

	//TODO Injetar dependencia
	SmsMessageDAO dao; 
	
	@Before
	public void init() {
		dao = new SmsMessageDAOImpl();
	}

	@Test
	public void persistTest() {
		SmsMessage sms = new SmsMessage("+5521987654321", "+5511988776655", "Mensagem dentro do limite"); 
		sms = dao.create(sms);
		assertNotNull(sms.getId());
	}
	
	@Test
	public void persistWithValidityTest() {
		Calendar cal = Calendar.getInstance(); 
		cal.set(2017, Calendar.JANUARY, 01);
		Date val = cal.getTime();
		
		SmsMessage sms = new SmsMessage("+5521987654321", "+5511988776655", "Mensagem dentro do limite", val); 
		sms = dao.create(sms);
		assertNotNull(sms.getDtValidity());
	}
	
	@Test
	public void retrieveTest() {
		SmsMessage sms = dao.retrieve(1);
		assertNotNull(sms);
	}
	
}
