package br.com.m4u.rh.domain;

import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.Test;

public class SmsMessageValidationTest {

	private Validator validator;
	
	@Before
	public void init() {
		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		this.validator = validatorFactory.getValidator();
	}
	
	@Test
	public void validationOKtest() {
		SmsMessage message = new SmsMessage(); 
		message.setTo("+5521987654321");
		message.setFrom("+5511988776655");
		message.setBody("Mensagem dentro do limite");
		
		Set<ConstraintViolation<SmsMessage>> violations = this.validator.validate(message);
		assertTrue(violations.isEmpty());
	}
	
	@Test
	public void validationNotNullToTest() {
		SmsMessage message = new SmsMessage(); 
		message.setTo(null);
		message.setFrom("+5511988776655");
		message.setBody("Mensagem dentro do limite");
		
		Set<ConstraintViolation<SmsMessage>> violations = this.validator.validate(message);
		
		String error = null;
		for (ConstraintViolation<SmsMessage> constraintViolation : violations) {
			error = constraintViolation.getMessage();
		}
		
		assertTrue(error.contains("may not be null"));
	}
	
	@Test
	public void validationNotNullFromTest() {
		SmsMessage message = new SmsMessage(); 
		message.setTo("+5521987654321");
		message.setFrom(null);
		message.setBody("Mensagem dentro do limite");
		
		Set<ConstraintViolation<SmsMessage>> violations = this.validator.validate(message);
		
		String error = null;
		for (ConstraintViolation<SmsMessage> constraintViolation : violations) {
			error = constraintViolation.getMessage();
		}
		
		assertTrue(error.contains("may not be null"));
	}
	
	@Test
	public void validationMoreThan160CharOnBodyTest() {
		SmsMessage message = new SmsMessage(); 
		message.setTo("+5521987654321");
		message.setFrom("+5511988776655");
		message.setBody("Mensagem fora do limite Mensagem fora do limite Mensagem fora do limite Mensagem fora do limite Mensagem fora do limite Mensagem fora do limite Mensagem fora do limite ");
		
		Set<ConstraintViolation<SmsMessage>> violations = this.validator.validate(message);
		
		String error = null;
		for (ConstraintViolation<SmsMessage> constraintViolation : violations) {
			error = constraintViolation.getMessage();
		}
		
		assertTrue(error.contains("size must be between 0 and 160"));
	}

	
}
