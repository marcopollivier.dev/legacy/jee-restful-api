package br.com.m4u.rh.persistence;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Test;

public class EntityManagerConnectionTest {

	@Test
	public void connectionTest() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("smssender");
		factory.close();
	}

}
