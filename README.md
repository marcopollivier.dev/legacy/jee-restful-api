# Projeto API SMS

## Marco Paulo Ollivier - marcopollivier@gmail.com

## Informações

### Infraestrutura
	- O projeto foi desenvolvido para ser executado no Apache Tomcat v8.0;

	- Há dois projetos no repositório:
		1) sms-sender-api - projeto principal;
		2) mock-operadora - projeto para simular a operadora que será requisitada pela nossa API.

	- Os dois projetos utilizam MVN para sua gerência de dependência
		* Para que os testes unitários funcionem corretamente o mock PRECISA estar estartado

	- O BD utilizado é um banco em memória (H2)

	- O projeto está versionado no GIT.


### Utilização
	- Efetuando um POST na API:
		1) Uma chamada  REST (JSON) é feita para API
		2) A API valida as regras de megócio
			2.1. A data de envio não pode ser maior que a data de validade
        3) A mensagem é enviada para a OPERADORA (mock)
        4) A mensagem é persistida

        * É possível passar a data de validade por parâmetro    

    - Efetuando um GET na API:
        1) Informar o ID e será retornada uma mensagem persistida.

### Exemplos de chamadas

#### API SMS
    * GET
	    http://localhost:8080/sms-sender-api/rest/sms/19

    * POST (application/json)
	    http://localhost:8080/sms-sender-api/rest/sms

#### MOCK Operadora
    * PUT (application/JSON) - Seguindo a documentação
	    http://localhost:8080/mock-operadora/rest/sms

#### Modelo do JSON API
    {
    	"from": "+5521987654321",
    	"to": "+5511988776655",
    	"body": "Mensagem dentro do limite"
    }

		{
    	"from": "+5521987654321",
    	"to": "+5511988776655",
    	"body": "Mensagem dentro do limite e com data",
			"dtValidity": "2020-01-01"
    }

#### Modelo do JSON MOCK OPERADORA
    {
    	"from": "+5521987654321",
    	"to": "+5511988776655",
    	"body": "Mensagem enviada para o mock da operadora"
    }

---
#TODO

- Mudar o servidor de aplicação para Glassfish
- Configurar CDI
